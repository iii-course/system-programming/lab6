# lab6

## Description
This is an education project written in C.

## Installation
To compile the server, run the following command:
```sh
make server
```

## Usage

### Run
To run the program, use this command:
```sh
./server -p 3217 -q 10
```

To clean all *.o files, run:
```sh
make clean
```