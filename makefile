CC=gcc
CFLAGS=-Wall -Werror -g -ggdb
LIBS=-lrt
SRC=src
SERVER_O=daemon.o files.o formatting.o logging.o socket.o server.o
CLIENT_O=formatting.o logging.o socket.o client.o

server: $(SERVER_O)
	$(CC) $(CFLAGS) $(SERVER_O) -o server
	
server.o: $(SRC)/server.c
	$(CC) $(CFLAGS) -c $(SRC)/server.c

socket.o: $(SRC)/socket/socket.c
	$(CC) $(CFLAGS) -c $(SRC)/socket/socket.c 

logging.o: $(SRC)/logging/logging.c
	$(CC) $(CFLAGS) -c $(SRC)/logging/logging.c 

formatting.o: $(SRC)/formatting/formatting.c
	$(CC) $(CFLAGS) -c $(SRC)/formatting/formatting.c

daemon.o: $(SRC)/daemon/daemon.c
	$(CC) $(CFLAGS) -c $(SRC)/daemon/daemon.c

files.o: $(SRC)/files/files.c
	$(CC) $(CFLAGS) -c $(SRC)/files/files.c

clean:
	rm -rf *.o server client
