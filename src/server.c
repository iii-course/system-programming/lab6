#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <errno.h> 
#include <sys/wait.h>

#include "./files/files.h"
#include "./daemon/daemon.h"
#include "./socket/socket.h"
#include "./logging/logging.h"
#include "./formatting/formatting.h"

short QUEUE_LENGTH = 10;
unsigned short PORT = 3217;
const int REQUEST_SIZE = 1024;
const int RESPONSE_SIZE = 4096;
const int RESPONSE_BODY_SIZE = 256;

char *filename = "file.txt";
char *log_filename = "server_log.txt";

void write_response(char *request, int peer_socket, int log_fd);
void write_error(int peer_socket, int log_fd);

/*
-p <PORT> 
-q <QUEUE> 
*/

int main(int argc, char const *argv[]) {
    if (argc < 5) {
        printf("Required arguments are not provided");
        exit(EXIT_FAILURE);
    }

    for (int i = 1; i < 4; i += 2) {
        if (strcmp(argv[i], "-p") == 0) {
            PORT =  strtol(argv[i + 1], NULL, 10);
        } else if (strcmp(argv[i], "-q") == 0) {
            QUEUE_LENGTH = strtol(argv[i + 1], NULL, 10);
        } else {
            printf("Unknown option: %s", argv[1]);
            exit(EXIT_FAILURE);
        }
    }

    int server_socket, peer_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    pid_t pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    set_up_daemon();

    int log_fd = open_for_writing(log_filename);
    write_process_info_to_log(log_fd);
    
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1) {
        exit_with_logging(EXIT_FAILURE, "\nError creating socket", log_fd);
    }
    
    set_up_address(&address, INADDR_ANY, PORT);
    
    start_socket(server_socket, address, QUEUE_LENGTH, log_fd);

    write_to_log(log_fd, "\nServer is listening\n");

    while(1) {
        write_to_log(log_fd, "\nWaiting for new connection...\n");
        if ((peer_socket = accept(server_socket, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            exit_with_logging(EXIT_FAILURE, "\nError accepting", log_fd);
        }
        
        char request[REQUEST_SIZE];
        read(peer_socket, request, REQUEST_SIZE);

        write_response(request, peer_socket, log_fd);

        write_to_log(log_fd, "\nMessage sent to client\n");
        close(peer_socket);
    }
    return 0;
}

void write_response(char *request, int peer_socket, int log_fd) {
    char response[4096] = {0};
    char body[256] = {0};
    read_from_file(filename, body);

    print_to_buffer_formatted_string(
        response, 
        "HTTP/1.1 200 OK\nContent-Type: text/plain\nContent-Length: %d\n\n%s",
        RESPONSE_BODY_SIZE, body
    );

    send_message(peer_socket, response, RESPONSE_SIZE, log_fd);
}

void write_error(int peer_socket, int log_fd) {
    char response[33] = "HTTP/1.1 503 Service Unavailable";
    send_message(peer_socket, response, 33, log_fd);
}
