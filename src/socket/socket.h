#pragma once

void set_up_address(struct sockaddr_in *address, int s_addr, int port);

void start_socket(int socket, struct sockaddr_in address, int queue_length, int log_fd);
void close_socket(int socket, int log_fd);

void recieve_message(int socket, char *buffer, int bytes_to_receive, int log_fd);
void send_message(int socket, char *message, int bytes_to_send, int log_fd);

bool should_keep_connection(char *request_message);
