#pragma once

void write_process_info_to_log(int log_fd);
void write_to_log(int log_fd, char *message);
void exit_with_logging(int status, char *message, int log_fd);
