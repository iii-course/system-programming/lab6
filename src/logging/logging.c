#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include "logging.h"
#include "../formatting/formatting.h"

void exit_with_logging(int status, char *message, int log_fd) {
    write_to_log(log_fd, message);
    close(log_fd);
    exit(status);
}

void write_process_info_to_log(int log_fd) {
    char buffer[512];
    write_process_info_to_buffer(buffer);
    write_to_log(log_fd, buffer);
}

void write_to_log(int log_fd, char *message) {
    if (write(log_fd, message, strlen(message)) == -1) {
        write(log_fd, strerror(errno), strlen(strerror(errno)));
        exit(EXIT_FAILURE);
    }
}