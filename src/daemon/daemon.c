#include "daemon.h"
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

void close_all_fds();
void redirect_standard_fds(void);

void set_up_daemon() {
    umask(0);
            
    pid_t sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    
    if ((chdir("./root")) < 0) {
        exit(EXIT_FAILURE);
    }
    
    close_all_fds();
    redirect_standard_fds();
}

void close_all_fds() {
    int max_fd = sysconf(_SC_OPEN_MAX); // getdtablesize();
    for (int i = 0; i < max_fd; ++i) {
        close(i);
    }
}

void redirect_standard_fds() {
    // 0 - stdin, 1 - stdout, 2 - stderr
    char DEV_NULL[10] = "/dev/null";
    if (open(DEV_NULL, O_RDONLY) == -1 || 
    open(DEV_NULL, O_WRONLY) == -1 || 
    open(DEV_NULL, O_WRONLY) == -1) {
        exit(EXIT_FAILURE);
    }
}