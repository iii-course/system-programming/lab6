#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>

#include "formatting.h"

void print_to_buffer_formatted_string(char *buffer, const char *format, ...) {
  va_list args;
  va_start(args, format);
  vsprintf(buffer, format, args);
  va_end(args);
}

void write_process_info_to_buffer(char *buffer) {
    pid_t pid = getpid();
    pid_t gpid = getgid();
    pid_t spid = getsid(pid);
    print_to_buffer_formatted_string(
        buffer, 
        "\nProcess ID: %d, process group ID: %d, process session ID: %d", 
        pid, gpid, spid
    );
}