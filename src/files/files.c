#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "files.h"

int open_for_writing(char *filename) {
    int fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, 0777);
    if (fd == -1) {
        exit(EXIT_FAILURE);
    }
    return fd;
}

void read_from_file(char *filename, char *buffer) {
    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        exit(EXIT_FAILURE);
    }

    // https://www.fundza.com/c4serious/fileIO_reading_all/index.html

    fseek(file, 0L, SEEK_END);
    long numbytes = ftell(file);

    fseek(file, 0L, SEEK_SET);	
    
    if (buffer == NULL) {
        exit(EXIT_FAILURE);
    }

    fread(buffer, sizeof(char), numbytes, file);
    fclose(file);
}